'''
Busqueda Binaria
Funciona unicamente cuando el conjunto de datos esta ordenado
'''

objetivo = int(input('Ingrese un numero: '))
epsilon = 0.01
limite_inf = 0.0
limite_sup = max(1.0, objetivo)
respuesta = (limite_sup+ limite_inf)/2

while abs(respuesta**2 - objetivo) >= epsilon:
    if respuesta**2 < objetivo:
        limite_inf = respuesta
    else:
        limite_sup = respuesta
    
    respuesta = (limite_sup + limite_inf) / 2


print(f'La raiz cuadrade de {objetivo} es {respuesta}')



