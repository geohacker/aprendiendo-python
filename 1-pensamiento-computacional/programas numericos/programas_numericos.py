def enumeracion(objetivo):
    respuesta = 0 
    while (respuesta**2 < objetivo):
        respuesta += 1
    if respuesta**2 == objetivo:
        print(f'La raiz cuadrada de {objetivo} es {respuesta}')
    else:
        print(f'{objetivo} no tiene enumeracion')



def aproximacion(objetivo):
    epsilon = 0.01
    paso = epsilon**2
    respuesta = 0.0 

    while abs(respuesta**2 - objetivo ) >= epsilon and respuesta <= objetivo:
        respuesta += paso

    if abs(respuesta**2 - objetivo) >= epsilon:
        print(f'no se encontro la raiz cuadrada del {objetivo}')
    else:
        print(f'La raiz cuadrada de {objetivo} es {respuesta}')

def bbinario(objetivo):
    epsilon = 0.01
    limite_inf = 0.0
    limite_sup = max(1.0, objetivo)
    respuesta = (limite_sup+ limite_inf)/2

    while abs(respuesta**2 - objetivo) >= epsilon:
        if respuesta**2 < objetivo:
            limite_inf = respuesta
        else:
            limite_sup = respuesta
        
        respuesta = (limite_sup + limite_inf) / 2


    print(f'La raiz cuadrade de {objetivo} es {respuesta}')


print('Encontrar la Raiz Cuadrada de un Número')

objetivo = int(input('Escoge un entero: '))

print(''' Ingrese el número 3 método a utilizar

1 - Enumeración
2 - Aproximación
3 - Búsqueda Binaria

''')

option = int(input('Input a option to do Algorithm: '))


if option == 1:
    enumeracion(objetivo)
elif option == 2:
    aproximacion(objetivo)
elif option == 3:
    bbinario(objetivo)
else:
    print('Mehh, Option failed, subnormal.')




