import numpy as np


#Crear Datos Random
np.random.seed(0) #Este valor establece la semilla de los "datos aleatorios" que va a generar
# Generar datos normales (media, desviacion estandar cantidad datos), Cantidad de decimales
estaturasHombres=np.round(np.random.normal(1.72,0.14,5000),2)
estaturasMujeres=np.round(np.random.normal(1.59,0.11,5000),2)

pesosHombres=np.round(np.random.normal(67,10,5000),2)
pesosMujeres=np.round(np.random.normal(61,7,5000),2)
